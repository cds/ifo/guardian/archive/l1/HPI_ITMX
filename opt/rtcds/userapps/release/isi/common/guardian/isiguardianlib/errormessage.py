class ErrorMessage(object):
    def __init__(self, message=""):
        self.message = message

    def __repr__(self):
        return 'ErrorMessage(%r)' % self.message

    def __str__(self):
        return self.message

    # FIXME: this is python2 and can be removed
    def __nonzero__(self):
        return bool(self.message)

    def __bool__(self):
        return self.message != ''

    def __add__(self, other):
        if isinstance(other, str):
            self.message += '\n' + other
        else:
            self.message += '\n' + other.message
        self.message = self.message.rstrip('\n')
        return self
